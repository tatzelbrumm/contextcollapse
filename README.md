# First proof-of-concept of miniedit.calliope.cc

This repository works on three levels:

1. Familiarization with [miniedit.calliope.com](https://miniedit.calliope.cc/) design flow

2. Monty Python's [Funniest Joke in the World](https://dai.ly/x2hwqqd)

3. Demonstration of the context of "[context collapse](https://web.archive.org/web/20181202202035/http://hlwiki.slais.ubc.ca/index.php/Context_collapse_in_social_media)" that [just cost Quinn Norton her job at the New York Times](https://www.wired.com/story/the-ny-times-fires-tech-writer-quinn-norton-and-its-complicated/) in such an elementary way that even elementary schools get it (not necessarily their teachers, though). 

## How the context collapse works

It's child's play to put the Funniest Joke in the World onto the calliope with the [Calliope mini editor](https://miniedit.calliope.cc/). 

[The challenge question of the joke is triggered by pressing button A. Afterwards, the evil Nazis are still alive.
The answer part of the joke is triggered by pressing button B. Afterwards, the evil Nazis are dead.](./videos/FunniestJoke.mp4)

[If adults don't get the joke and just see the end of the first part of the joke, they'll get very upset and go crazy, 
because they think that you're a Nazi when you play part A. This makes no sense at all.](./videos/ContextCollapse.mp4)

The adults' reaction to this exercise is the main take-out lesson for kids who don't know and don't care about History.

## Supporting material

* Monty Python's original [Funniest Joke in the World](https://dai.ly/x2hwqqd)

* [Lustigster Witz der Welt](https://youtu.be/BMjXX-ed0Pc) for a German audience

* A version for and by Viennese school children [Der lustigste Witz der Welt oder Julias 11. Geburtstagsparty](https://youtu.be/51kBzFGIrNU)

* A [fairytale version](https://www.publicmedievalist.com/sca-swastika/), lest some adults want a presentation "suitable for children"